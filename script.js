const quotes = [
    {
        name: 'Cheryl Maise Lobo',
        quote: "Take risks. You will either fail or pass but it'll be worth it"
    },
    {
        name: 'Jopaul John',
        quote: 'Good for you'
    },
    {
        name: 'Jopaul John',
        quote: 'Gofoyou'
    },
    {
        name: 'Leonardo da Vinci',
        quote: 'It had long since come to my attention that people of accompalishment rarely sat back and let things happen to them. They went out and happened to things.'
    },
    {
        name: 'William W. Purkey',
        quote: "You've gotta dance like there's nobody watching, Love like you'll never be hurt, Sing like there's nobody listening, And live like it's heaven on earth."

    },
    {
        name: 'Stephan King',
        quote: "Get busy living or get busy dying"
    }

]

const quoteBtn = document.querySelector('.#quoteBtn');
const quoteAuthor = document.querySelector('.#quoteAuthor');
const quote = document.querySelector('.#quote');

quoteBtn.addEventListener('click', displayQuote);

function displayQuote (){
    let number = Math.floor(Math.random()*quotes.length);
    quoteAuthor.innerHTML = quotes[number].name;
    quote.innerHTML = quotes[number].quote;

}

